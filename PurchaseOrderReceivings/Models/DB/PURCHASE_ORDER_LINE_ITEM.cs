//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PurchaseOrderReceivings.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class PURCHASE_ORDER_LINE_ITEM
    {
        public string Purchase_Order_Number { get; set; }
        public decimal Item_Sequence_Number { get; set; }
        public string Requisition_Dept_Number { get; set; }
        public string Requisition_Sub_Dept_Number { get; set; }
        public string Commodity_Code { get; set; }
        public string Legacy_Item_Code { get; set; }
        public string PPO_Item_Code { get; set; }
        public string Campaign_Of_Introduction { get; set; }
        public string Part_Number { get; set; }
        public string Item_Short_Name { get; set; }
        public string Item_Description_Text { get; set; }
        public Nullable<decimal> Pieces_Per_Package { get; set; }
        public Nullable<decimal> Quantity_Ordered_Count { get; set; }
        public Nullable<decimal> Quantity_Ordered_Final { get; set; }
        public Nullable<decimal> Unit_Price_Amount { get; set; }
        public Nullable<decimal> Extended_Price_Amount { get; set; }
        public string Unit_Of_Issue_Text { get; set; }
        public string Old_Commodity_Flag { get; set; }
        public Nullable<decimal> Commodity_Group_Code { get; set; }
    
        public virtual PURCHASE_ORDER PURCHASE_ORDER { get; set; }
    }
}
