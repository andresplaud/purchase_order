//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PurchaseOrderReceivings.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class COMPANY_ADDRESS
    {
        public string Ship_Address_Code { get; set; }
        public string Ship_Address_Type { get; set; }
        public string Comapany_Name { get; set; }
        public string Company_Address_Line1 { get; set; }
        public string Company_Address_Line2 { get; set; }
        public string Company_Address_Line3 { get; set; }
        public string Company_City { get; set; }
        public string Company_State_Code { get; set; }
        public string Company_Postal_Code { get; set; }
        public string Company_Telephone_Number { get; set; }
        public string Company_Fax_Number { get; set; }
    }
}
