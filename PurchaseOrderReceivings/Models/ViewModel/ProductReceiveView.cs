﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PurchaseOrderReceivings.Models.ViewModel
{
    public class ProductReceiveView
    {       

        public IEnumerable<ProductManifests> ProductManifestList { get; set; }

        public IEnumerable<ProductModel> ProductList { get; set; } 
    }
}