﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurchaseOrderReceivings.Models.ViewModel
{
    public class PurchaseOrder
    {
        public string Purchase_Order_Number { get; set; }
        public string Vendor_Number { get; set; }
        public string Purchase_Order_Document_Path { get; set; }

        public PurchaseOrder()
        {
            Purchase_Order_Number = string.Empty;
            Vendor_Number = string.Empty;
            Purchase_Order_Document_Path = string.Empty;
        }
    }
}