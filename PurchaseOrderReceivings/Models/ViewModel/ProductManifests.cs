﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseOrderReceivings.Models.ViewModel
{
    public class ProductManifests
    {
        [Display(Name = "Manifest Number")]
        public string Manifest_Number { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}")]
        [Display(Name = "Received Date")]
        public DateTime WH_Receipt_Date { get; set; }
        [Display(Name = "Product ID")]
        public string Product_Code { get; set; }
        [Display(Name = "Description")]
        public string Product_Description { get; set; }
        [Display(Name = "Qnty Received")]
        public decimal? Received_Quantity { get; set; }
    }
}