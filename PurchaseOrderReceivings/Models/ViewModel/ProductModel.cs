﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseOrderReceivings.Models.ViewModel
{
    public class ProductModel
    {
        [Key]
        [Display(Name = "Product ID")]
        [Required(ErrorMessage = "*Product Code Entry Required!")]       
        public string Product_Code { get; set; }
        [Display(Name = "Product Description:")]
        public string Product_Description { get; set; }
        

    }
}