﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PurchaseOrderReceivings.Models.DB;
using PurchaseOrderReceivings.Models.ViewModel;
using static PurchaseOrderReceivings.Models.ViewModel.ProductModel;

namespace PurchaseOrderReceivings.Models.EntityManager
{
    public class ProductManager
    {
        /// <summary>
        /// Get Purchase Order Info....
        /// </summary>
        /// <param name="OrderNum"></param>
        /// <returns></returns>
        public PurchaseOrder GetPurchaseOrder(string OrderNum)
        {
            PurchaseOrder PO = new PurchaseOrder();

            using (AVON_PurchaseOrdersEntities db = new AVON_PurchaseOrdersEntities())
            {
                var Order = db.PURCHASE_ORDER.Where(p => p.Purchase_Order_Number == OrderNum).Select(o => new PurchaseOrder
                {
                    Purchase_Order_Number = o.Purchase_Order_Number,
                    Vendor_Number = o.Vendor_Number,
                    Purchase_Order_Document_Path = o.Purchase_Order_Document_Path                
                }).FirstOrDefault();

                PO = Order;               
            }

            return PO;           
        }

        /// <summary>
        /// Get Manifests list by Product...
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public ProductReceiveView GetProductReceiveView(string productCode)
        {
            ProductReceiveView PRV = new ProductReceiveView();

            //List<ProductManifests> manifests = GetProductManifests(productCode);

            PRV.ProductManifestList = GetProductManifests(productCode); //manifests       

            return PRV;
        }

        private List<ProductManifests> GetProductManifests(string productCode)
        {
            using (AVON_PurchaseOrdersEntities db = new AVON_PurchaseOrdersEntities())
            {
                var manifests = db.ProductReceivingHist_View_1.Where(s => s.Product_Code == productCode).Select(o => new ProductManifests {
                    Manifest_Number = o.Manifest_Number,
                    WH_Receipt_Date = o.WH_Receipt_Date,
                    Received_Quantity = o.Received_Quantity,
                    Product_Code = o.Product_Code,
                    Product_Description = o.Product_Description
                }).ToList();


                return manifests;
            }
        }     

        /// <summary>
        /// Get Product Info...
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public ProductModel GetProduct(string productCode)
        {
            ProductModel PM = new ProductModel();

            using (AVON_PurchaseOrdersEntities db = new AVON_PurchaseOrdersEntities())
            {
                var PRD = db.PRODUCTs.Find(productCode);
                if (PRD != null)
                {                  
                    PM.Product_Code = PRD.Product_Code;
                    PM.Product_Description = PRD.Product_Description;
                }
            }

            return PM;
        }

        /// <summary>
        /// Get Product History List...
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public ProductReceiveView GetProductHistoryView(string productCode) 
        {
            ProductReceiveView PHST = new ProductReceiveView(); 

            //List<ProductModel> products = GetProductHist(productCode);

            PHST.ProductList = GetProductHist(productCode);  //products;

            return PHST;
        }
        private List<ProductModel> GetProductHist(string productCode)
        {
            //ProductReceiveView PHST = new ProductReceiveView();
            using (AVON_PurchaseOrdersEntities db = new AVON_PurchaseOrdersEntities())
            {
                var products = db.ProductReceivingHist_View_1.Where(s => s.Product_Code == productCode).Select(o => new ProductModel
                {
                    Product_Code = o.Product_Code,
                    Product_Description = o.Product_Description                   
                }).ToList();               

                return products;
            }
        }

        /// <summary>
        /// Get Manifest Products List...
        /// </summary>
        /// <param name="manifestNum"></param>
        /// <returns></returns>
        public List<ProductManifests> GetManifestProducts(string manifestNum)
        {
            using (AVON_PurchaseOrdersEntities db = new AVON_PurchaseOrdersEntities())
            {
                var products = db.ProductReceivingHist_View_1.Where(s => s.Manifest_Number == manifestNum).Select(o => new ProductManifests
                {
                    Manifest_Number = o.Manifest_Number,
                    WH_Receipt_Date = o.WH_Receipt_Date,
                    Received_Quantity = o.Received_Quantity,
                    Product_Code = o.Product_Code,
                    Product_Description = o.Product_Description
                }).ToList();


                return products;
            }

        }

    }
}