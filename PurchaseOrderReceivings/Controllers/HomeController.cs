﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurchaseOrderReceivings.Models.ViewModel;
using PurchaseOrderReceivings.Models.EntityManager;
using System.IO;

namespace PurchaseOrderReceivings.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Product() 
        {
            ViewBag.Message = "Your Product Search page.";
                      
            ProductModel prod = new ProductModel();
            //ProductManager PM = new ProductManager();
            //ProductModel PRD = PM.GetProduct(prod);

            return View( );
        }

        [HttpPost]
        public ActionResult Product(ProductModel product) 
        {
            try
            {
                ViewBag.Message = "Your Product Search page.";
                ProductManager PM = new ProductManager();
                ProductModel PRD = PM.GetProduct(product.Product_Code);

                //ProductManager PM = new ProductManager();
                ProductReceiveView PRV = PM.GetProductReceiveView(PRD.Product_Code.Trim());

                return View("ManageProductPartial", PRV.ProductManifestList);
            }
            catch (Exception ex)
            {
                product.Product_Description = "Not Found!!";
                return View(product);
            }
        }

        public ActionResult ManageProductPartial(ProductModel product)
        {
            //ProductManager PM = new ProductManager();

            //ProductReceiveView PRV = PM.GetProductReceiveView(product.Product_Code);


            ViewBag.Message = "Your Product Receiving History page.";

            return View();
        }

        //public ActionResult DownloadPO() 
        //{
        //    ProductManager PM = new ProductManager();

        //    PurchaseOrder PO = PM.GetPurchaseOrder("2000130");

        //    string dPath = PO.Purchase_Order_Document_Path;

        //    //string fileName = "2000130_2019-09-17ORD.pdf";
        //    //string path = Path.Combine(@"G:/Suppy Chain/PurchaseOrders/", fileName);

        //    return File(dPath, "application/pdf");

        //}

    }
}