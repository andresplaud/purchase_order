﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurchaseOrderReceivings.Models.ViewModel;
using PurchaseOrderReceivings.Models.EntityManager;

namespace PurchaseOrderReceivings.Controllers
{
    public class ReceivingsController : Controller
    {
        // GET: Receivings
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Receivings()
        {
            ProductManager PM = new ProductManager();

            ProductReceiveView PRV = new ProductReceiveView
            {
                ProductManifestList = PM.GetManifestProducts("")
            };


            return View(PRV.ProductManifestList);
        }

        [HttpPost]
        public ActionResult Receivings(ProductManifests manifest)
        {
            ProductManager PM = new ProductManager();

            ProductReceiveView PRV = new ProductReceiveView
            {
                ProductManifestList = PM.GetManifestProducts(manifest.Manifest_Number)
            };

            ViewBag.Message = "Your Product Receiving History page.";

            return View(PRV.ProductManifestList);
        }


        public ActionResult DownloadPO(ProductManifests manifest)
        {          
            ProductManager PM = new ProductManager();

            PurchaseOrder PO = PM.GetPurchaseOrder(manifest.Manifest_Number);

            if (PO != null)
            {
                string dPath = PO.Purchase_Order_Document_Path;
                return File(dPath, "application/pdf");
            }
            else
            {
                string fileName = "NoOrderDocumentFound.pdf";
                string path = System.IO.Path.Combine(@"G:/Suppy Chain/PurchaseOrders/", fileName);
                return File(path, "application/pdf");
            }                         
        }
    }
}